> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Taylor Marks

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1,2)

#### README.md file should include the following items:

* Screenshot of AMPPS installation My PHP Installation
* Screenshot of runniny java Hello
* Screenshot of running Android Studio
* git commands w/short descriptions
* Bitbucket repo links a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. Git init - creates new Git repository
2. Git status - displays the state of the working directory 
3. Git add - updates the index with current information
4. Git commit - saves changes to local repository
5. Git push - uploads local repository information to remote repository
6. git pull - runs git fetch with given parameters and merges retrieved heads into current branch
7. git clone - clones or copies an existing repository

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.PNG)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/java.PNG)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/firstapp.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jtmarks/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
