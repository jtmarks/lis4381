> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Taylor Marks

### Assignment #2 Requirements: Create a mobile recipe app using Android Studio

#### Assignment #2 Screenshots:

*Screenshot of App main page*:

![Bruschetta app main page Screenshot](img/Capture1.PNG)

*Screenshot of recipe page*:

![Recipe Screenshot](img/recipeIng.PNG)



