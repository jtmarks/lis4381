> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Taylor Marks

### Project 2 Requirements: 1. Add Delete and Edit Functions 2. RSS Feed

#### Project 2 Screenshots:

Screenshot of the RSS Feed:
![RSS Feed Screenshot](img/rss.PNG)

Screenshot of the Main Project 2 page:

![index for P2 Screenshot](img/main.PNG)

Screenshot of the Carousel page:

![carousel Screenshot](img/carousel.PNG)

Screenshot of the edit page:

![edit page Screenshot](img/edit.PNG)

Screenshot of the edit+process page:

![edit process page Screenshot](img/edit_process.PNG)