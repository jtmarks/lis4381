> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Taylor Marks

### Assignment #5 Requirements: 1.Create server-side validation

#### Assignment #5 Screenshots:

link to local app:
(http://localhost/repos/lis4381/a5/index.php)

Screenshot of the index page:
![Page1 Screenshot](img/add.png)

Screenshot of the error page:

![Page2 Screenshot](img/error.png)