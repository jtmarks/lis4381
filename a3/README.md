> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Taylor Marks

### Assignment #3 Requirements:Create a mobile recipe app using Android Studio

#### Assignment #3 Screenshots:

*Screenshot of ERD*:
![ERD Screenshot](img/a3.png)

*Screenshot of App main page*:

![Concert app main page Screenshot](img/Capture1.PNG)

*Screenshot of recipe page*:

![Concert Ticket price Screenshot](img/Capture2.PNG)

Links:

[a3 mwb file](a3/a3.mwb "My a3 mwb file")

[a3 sql file](a3/a3.sql "My a3 sql file")