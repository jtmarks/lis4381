# LIS 4381 - Mobile Web Application Development

## Taylor Marks

### Assignment 1 Requirements:

*Course Work Links (Relative link example):*

1. [a1 README.md](a1/README.md "My a1 README.md file")
    - Install AMPPS 
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete bitbucket tutorials
    - Provide git command descriptions

2. [a2 README.md](a2/README.md "My a2 READ.md file")
    - Create a mobile recipe app using Android Studio
    - Provide screenshots of the completed app

3. [a3 README.md](a3/README.md "My a3 READ.md file")
    - Created concert app using buttons
    - Used MySQL to create a database

4. [p1 README.md](p1/README.md "My p1 READ.md file")
    - Created mobile business card app
    - Share information with future employeers

5. [a4 README.md](a4/README.md "My a4 READ.md file")
    - client side validation
    - created mysql database

6. [a5 README.md](a5/README.md "My a5 READ.md file")
    - server side validation
    - used data from a4   

7. [p2 README.md](p2/README.md "My p2 READ.md file")
    - server side validation
    - used data from a5 to add delete/ edit functions
    - implemented RSS Feed