<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Jacqueline Marks">
    <link rel="icon" href="favicon.ico">

		<title>LIS 4381 - Project 1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> 
					Create a business card with active button and information for potential employers.
					*Create a launcher icon and display it
					*Must add background color to both activities
					*Must add border around image and button
					*Must add text shadow
				</p>

				<h4>Front of Business Card</h4>
				<img src="img/Capture1.PNG" class="img-responsive center-block" alt="capture1">

				<h4>Business Information</h4>
				<img src="img/Capture2.PNG" class="img-responsive center-block" alt="capture2">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
