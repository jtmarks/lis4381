> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Taylor Marks

### Project #1 Requirements: 1.Create a launcher icon image and display it in both activities (screens)2.Must add background color(s) to both activities3.Must add border around image and button4.Must add text shadow(button)

#### Assignment #4 Screenshots:

*Screenshot of the main page*:
![Page1 Screenshot](img/Capture1.png)

*Screenshot of the second page*:

![Page2 Screenshot](img/Capture2.PNG)

